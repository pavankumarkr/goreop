FROM golang:1.13
MAINTAINER "pavankumar"
ENV GOPATH /go
RUN mkdir -p $GOPATH/{src,pkg,bin}
ENV PATH $PATH:/usr/local/go/bin:$GOPATH/bin
RUN cd /go/src/
WORKDIR $GOPATH/src
RUN git clone https://pavankumarkr@bitbucket.org/pavankumarkr/goreop.git
RUN env GOOS=linux GOARCH=amd64 go install goreop

