# docker-centos7-omniaclientCICD

Base image for omniaclient

golang : version --> 1.13


#  Building
```bash
docker build -t docker-centos7-omniaclient:latest -f $WORKSPACE/docker-centos7-omniaclientCICD/Dockerfile .
```

# Usage
Run the shell
```bash
docker run --privileged  --rm=true --name omnia-client docker-centos7-omniaclient:latest
```
## Maintainer
cpd-dsps-dsdg <cpd-dsps-dsg-all@mail.rakuten.com>